import os
import cv2
import csv
import numpy as np
from tqdm import tqdm
import skimage.filters
import skimage.morphology

def floor(image):
    nbins = 1000
    hist = np.histogram(image, bins=nbins)
    #print(hist[0][:300])

    thresh = np.prod(image.shape)//4

    total = 0
    for pop, edge in zip(*hist):
        total += pop
        if total > thresh:
            #print('floor:', edge, 'min:', np.min(image))
            return edge

    assert(False)
    return 0

def gen_mask(image, thresh=0.1):
    image = skimage.filters.gaussian(image,4)

    #thresh = #skimage.filters.threshold_li(image)
    #print("Thresholding at ",thresh)
    mask = image > thresh

    area = np.prod(image.shape)
    bigstuff = skimage.morphology.remove_small_objects(mask, min_size=area/6)

    #plt.imshow(bigstuff); plt.show()

    smallstuff = np.logical_and(mask, np.logical_not(bigstuff))
    smallstuff = skimage.morphology.binary_dilation(smallstuff, selem=skimage.morphology.disk(5))

    mask = np.logical_not(smallstuff)

    return mask

def show_pair(before, after):
    diff = after-before

    plt.figure( figsize=(20,20))
    plt.imshow(np.concatenate((after,diff), axis=1))
    plt.axis('off')

    #fig, ax = skimage.filters.try_all_threshold(image)
    plt.show()

def reshape(image):
    h, w = image.shape
    image = image.astype(float)

    image -= floor(image)
    image  = np.maximum(image, 0)
    image /= np.max(image)

    # Flip image if it points to the left
    wmap = np.mean(image, axis=0)
    assert(len(wmap)==w)
    #print(weight)

    if sum(wmap[0:w//2]) < sum(wmap[w//2:w]):
        image  = np.fliplr(image)
        wmap = wmap[::-1]

    # Trim white frame around (if any)
    #wmap = np.median(image[h*20//100:h*80//100,:], axis=0)
    assert(len(wmap)==w)

    l = 0
    while l < w*20//100 and np.mean(wmap[l:l+10]) > .6:
        l += 1

    wthresh = np.min(wmap)+.005

    r = w
    for rr in range(w, w*8//10, -1):
        if np.median(wmap[rr-10:rr]) <= wthresh:
            r = rr

    hmap = np.mean(image[:,l:r], axis=1)
    assert(len(hmap)==h)

    t = 0
    while t < h*20//100 and np.median(hmap[t:t+10]) > .6:
        t += 1

    if t != 0:
        t += h//200

    hthresh = np.min(hmap)+.005
    #print(hmap[-100:])

    b = h
    for bb in range(h, h*8//10, -1):
        if np.mean(hmap[bb-10:bb]) <= hthresh:
            b = bb

    #print(hmap[b-10:b])

    # Crop the image
    image = image[t:b, l:r]
    w = r-l
    h = b-t

    if w > h//2:
        # Fit width of the fatty image
        w, h = 1024, h*1024//w
    else:
        # Fit height of a skinny image
        w, h = w*2048//h, 2048

    image = cv2.resize(image, (w,h), cv2.INTER_AREA)
    #print(image.shape)

    #plt.imshow(image); plt.show()

    mask = gen_mask(image)
    #plt.imshow(mask); plt.show()

    #plt.show(mask); plt.show()

    image *= mask

    image -= floor(image)
    image  = np.maximum(image, 0)
    image /= np.max(image)

    if h < 2048:
        # Pad fatty image equally on top and bottom
        image = np.pad(image, (((2048-h)//2, (2048+1-h)//2),(0,0)), 'constant')
    elif w < 1024:
        # Pad skinny image on the right
        image = np.pad(image, ((0,0), (0, 1024-w)), 'constant')

    assert(image.shape == (2048, 1024))

    return image

def convert_images(src_folder, dst_folder, ext, csv_name=None):

    image_files = sorted(os.listdir(src_folder))

    if not os.path.exists(dst_folder):
        print("Creating " + dst_folder)
        os.makedirs(dst_folder)

    csvfile = open(os.path.join(data_folder, csv_name), 'w', newline='') if csv_name else None
    writer = None
    if not csvfile is None:
        cols = ['File', 'Rows', 'Cols', 'Bits', 'Min', 'Max']
        writer = csv.DictWriter(csvfile, fieldnames=cols)
        writer.writeheader()

    def load_and_log(name):
        #print(name)
        image_path = os.path.join(src_folder, name)
        image = cv2.imread(image_path, cv2.IMREAD_ANYDEPTH)
        if image is None:
            return image

        if not writer is None:
            row = {
                'File': name,
                'Rows': image.shape[0],
                'Cols': image.shape[1],
                'Bits': image.dtype,
                'Min': np.min(image),
                'Max': np.max(image),
            }

            writer.writerow(row)

        return image

    paired = None
    progress = tqdm(image_files)
    for src_name in progress:
        if not ext in src_name:
            continue

        if src_name == paired:
            continue

        progress.set_description(src_name)

        CC  = None
        MLO = None

        if "CC" in src_name:
            CC  = load_and_log(src_name)
            paired = src_name.replace('CC', 'MLO')
            MLO = load_and_log(paired)
        else:
            CC  = None
            paired = None
            MLO  = load_and_log(src_name)

        if not CC is None and not MLO is None:
            assert('_CC' in src_name)
            png_path = os.path.join(dst_folder, src_name.replace('_CC', '').replace(ext,'png'))

            test = cv2.imread(png_path, cv2.IMREAD_ANYDEPTH)
            if (not test is None) and test.shape == (2048, 2048):
                continue

            CC  = reshape(CC)
            MLO = reshape(MLO)

            pair = np.concatenate((CC, MLO), axis=1)
            assert(pair.shape ==(2048, 2048))

            pair = (pair*65535).astype(np.uint16)

            cv2.imwrite(png_path, pair, [cv2.IMWRITE_PNG_COMPRESSION, 9])

def tiff_to_png(data_folder, csv_name=None):
    src_folder = os.path.join(data_folder, 'tif')
    dst_folder = os.path.join(data_folder, 'png')

    convert_images(srt_folder, dst_folder, 'tif', csv_name)


#tiff_to_png('../data/ddsm')#, 'ddsm.csv')

def case_to_images(case_folder, dst_folder, out_format='png'):
    files = os.listdir(case_folder)

    def get_file(token):
        for file in files:
            if '.1' in file or '.2' in file:
                continue

            if token in file:
                return file

        return None

def parse_ics(ics_path):
    def parse_mode(tokens):
        result = {}
        for key, value in zip(tokens[0::2], tokens[1::2]+[""]):
            if key == 'LINES':
                result['height'] = int(value)
            elif key == 'PIXELS_PER_LINE':
                result['width'] = int(value)
            elif key == 'BITS_PER_PIXEL':
                result['precision'] = int(value)
            elif key == 'NON_OVERLAY':
                result['overlay'] = False
            elif key == 'OVERLAY':
                result['overlay'] = True

        return result


    def parse_tokens(tokens):
        head, *tail = tokens

        if head == 'DIGITIZER':
            return {'digitizer': ' '.join(tail).strip()}

        if head in ('LEFT_CC','LEFT_MLO','RIGHT_CC','RIGHT_MLO'):
            return { head: parse_mode(tail)}

        return {}

    ics = {}
    for line in open(ics_path):
        ics.update(parse_tokens(line.strip().split(' ')))

    return ics

def case_to_images(case_folder, dst_folder, out_format='png'):
    files = os.listdir(case_folder)

    def get_file(token):
        for file in files:
            if '.1' in file:
                continue

            if token in file:
                return file

        return None

    ics = parse_ics(os.path.join(case_folder, get_file('.ics')))

    gamma = {
        'DBA 21':([0,185,271,393,581,847,1217,1772,2603,3797,5648,8296,12569,18445,26986,37201,55722,65535],
                  [0,22391,25304,27852,31129,33860,36590,39321,42234,44964,48059,50608,52974,56433,59346,61712,64807,65535]),
    }

    def raw_to_image(mode):
        header = ics[mode]
        if header is None:
            print("Warning! '"+mode+"' is not listed in "+case_folder+"/*.ics")
            return

        ljpeg_name = get_file(mode + '.LJPEG')
        if ljpeg_name is None:
            print("Warning! '"+ljpeg_name+"' is not found in "+case_folder)
            return

        precision = header['precision']
        dtype = np.uint16 if precision > 8 else np.uint8
        size = (header['height'],header['width'])

        ljpeg_path = os.path.join(case_folder, ljpeg_name)
        img_path = os.path.join(dst_folder, ljpeg_name.replace('LJPEG', out_format))

        existing = cv2.imread(img_path, cv2.IMREAD_ANYDEPTH)
        if not( existing is None ) and existing.shape == size and existing.dtype == dtype:
            return

        os.system('/home/ksebov/_dev/JPEGv1.2.1_LINUXport/jpegdir/jpeg -d -s "' + ljpeg_path +'" >/dev/null 2>&1')

        raw_path = ljpeg_path + '.1'

        buffer = np.fromfile(raw_path, dtype=dtype)
        if buffer is None:
            return

        assert(len(buffer)==size[0]*size[1])
        buffer.byteswap(inplace=True)

        digitizer = ics['digitizer']
        if digitizer in gamma:
            xp, fp = gamma[digitizer]
            buffer = np.interp(buffer, xp, fp).astype(np.uint16)
        elif precision in range(8, 16):
            buffer *= 2**(16-precision)

        assert(len(buffer)==size[0]*size[1])

        #print(np.min(buffer), np.max(buffer))

        cv2.imwrite(img_path, buffer.reshape(size), [cv2.IMWRITE_PNG_COMPRESSION, 9])

        os.remove(raw_path)

    raw_to_image('LEFT_CC')
    raw_to_image('LEFT_MLO')
    raw_to_image('RIGHT_CC')
    raw_to_image('RIGHT_MLO')


def cases_to_images(group_folder, dst_folder, out_format='png'):
    cases = sorted(os.listdir(group_folder))

    progress = tqdm(cases)
    for case in progress:
        case_folder = os.path.join(group_folder, case)
        if not os.path.isdir(case_folder):
            continue

        progress.set_description(case)
        case_to_images(case_folder, dst_folder, out_format)

def summarize_ics(dir, csv_name='summary.csv'):
    files = sorted(os.listdir(dir))

    cols = ['File', 'Rows', 'Cols', 'Bits']
    csvfile = open(os.path.join(dir, csv_name), 'w', newline='')

    writer = csv.DictWriter(csvfile, fieldnames=cols)

    writer.writeheader()

    progress = tqdm(files)
    for file in progress:
        if not '.ics' in file:
            continue

        progress.set_description(file)

        ics = parse_ics(os.path.join(dir, file))

        def log_mode(mode):
            header = ics[mode]
            if header is None:
                print("Warning! '"+mode+"' not found in"+file)

                return

            row = {
                'File': file,
                'Rows': header['height'],
                'Cols': header['width'],
                'Bits': header['precision'],
            }

            writer.writerow(row)

        log_mode("LEFT_CC")
        log_mode("LEFT_MLO")
        log_mode("RIGHT_CC")
        log_mode("RIGHT_MLO")

#summarize_ics('../data/ddsm/ics')
import matplotlib.pyplot as plt

def show_file_mask(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_ANYDEPTH)
    if image is None:
        print("Warning! Couldn't load "+image_path)

        return

    image = image.astype(np.float32)/65535
    #if not (np.min(image)<.1 and np.max(image)>.9):
    print(np.min(image), np.max(image))

    image = image[:,0:1024]
    masked = image*gen_mask(image)

    show_pair(image, masked)

def show_dir_mask(dir):
    files = sorted(os.listdir(dir))

    progress = tqdm(files)
    for file in progress:
        if 'mask' in file:
            continue

        progress.set_description(file)

        show_file_mask(os.path.join(dir,file))

import glob
import random

def convert_labels(src_csv, image_dir, ext):
    image_filenames = sorted((os.listdir(image_dir)))

    csvfile = open(os.path.join(image_dir, "labels.csv"), 'w', newline='')
    cols = ['file', 'rand', 'label']

    writer = csv.DictWriter(csvfile, fieldnames=cols)
    writer.writeheader()

    reader = csv.DictReader(open(src_csv, newline=''))

    CC = None

    rows = 0
    labeled = 0
    not_found = 0

    for row in reader:
        rows += 1
        src_name = row['name']

        if "_CC" in src_name:
            if not CC is None:
                print("Unexpected file!", src_name)
                not_found += 1

            CC = row.copy()
            continue

        if "_MLO" in src_name:
            if CC is None or src_name != CC['name'].replace("_CC", "_MLO"):
                print("Unexpected file!", src_name)
                not_found += 1
                continue

        if row["label"] != CC['label']:
            print("Unexpected label!", src_name)

            CC = None
            continue

        CC = None

        src_name = src_name.replace("_MLO", "") + "." + ext
        if not os.path.isfile(os.path.join(image_dir, src_name)):
            #print("File not found! ", src_name)
            not_found += 1

            continue

        image_filenames.remove(src_name)
        writer.writerow({"file":src_name, "label": row['label'], "rand":random.random()})
        labeled += 1

    unlabeled = 0
    for image in image_filenames:
        if ext in image:
            print("Left unlabeled:", image)
            unlabeled += 1

    print("Total processed: %i, (labeled: %i, not_found: %i), unlabeled: %i"%((rows, labeled, not_found, unlabeled)))

def translate_pathology(pathology):
    if pathology == "BENING_WITHOUT_CALLBACK":
        return "BWC"
    
    return pathology

def parse_overlay(overlay_path):
    if overlay_path is None:
        return { 'abnormalities': "", 'pathologies':""}

    pathologies = set()
    result = {'abnormalities': 0}

    def parse_tokens(tokens):
        head, *tail = tokens

        if head == 'TOTAL_ABNORMALITIES':
            result['abnormalities'] = int(tail[0])
        elif head == 'PATHOLOGY':
            pathologies.add(translate_pathology(' '.join(tail).strip()))

    for line in open(overlay_path):
        parse_tokens(line.split(' '))

    result['pathologies'] = ', '.join(pathologies)
    
    return result

def scan_overlays(cases_dir, csv_file):
    csvfile = open(csv_file, 'w', newline='')
    cols = ['file', 'case', 'CC abnormalities', 'CC pathologies', 'MLO abnormalities', 'MLO pathologies']

    writer = csv.DictWriter(csvfile, fieldnames=cols)
    writer.writeheader()

    def scan_case(case):
        case_dir = os.path.join(cases_dir, case)
        ics_filenames = sorted(glob.glob(os.path.join(case_dir, '**/*.ics'), recursive=True))

        for ics_filename in ics_filenames:
            ics = parse_ics(os.path.join(case_dir, ics_filename))

            def scan_side(side):
                case_name = ics_filename.replace('-','_').replace("ics", side)

                def overlay_path(mode):
                    mode_attrs = ics[side+mode]
                    if not mode_attrs['overlay']:
                        return None

                    return os.path.join(case_dir, case_name + mode + '.OVERLAY')
               
                CC  = parse_overlay(overlay_path("_CC"))
                MLO = parse_overlay(overlay_path("_MLO"))

                row = {
                    'file':os.path.basename(case_name),
                    'case':case,
                    'CC abnormalities' : CC ['abnormalities'],
                    'CC pathologies'   : CC ['pathologies'],
                    'MLO abnormalities': MLO['abnormalities'],
                    'MLO pathologies'  : MLO['pathologies'],
                    }

                writer.writerow(row)
            
            scan_side('LEFT')
            scan_side('RIGHT')

    scan_case('normals')
    scan_case('benign_without_callbacks')
    scan_case('benigns')
    scan_case('cancers')


#show_file_mask('../data/normals/A_0022_1.LEFT.png')

#show_dir_mask('../data/test/')

"""
test = cv2.imread('/home/ksebov/_bits/DDSM/png/normals/A_0024_1.LEFT_CC.png', cv2.IMREAD_ANYDEPTH)
assert(not test is None)
test = reshape(test)
plt.figure( figsize=(20,20))
plt.imshow(test)
plt.show()
"""
#convert_images('/home/ksebov/_bits/DDSM/png/benign_without_callbacks', '../data/benign_without_callbacks', 'png')

#convert_labels('../data/ddsm-709/labels.dropbox.csv', '../data/ddsm-709/png', 'png')

scan_overlays('/home/ksebov/_bits/DDSM/cases', '/home/ksebov/_bits/DDSM/cases/overlays.csv')