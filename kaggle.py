import pydicom
import csv
import os
from tqdm import tqdm

def write_stats(data_path, csv_name):
    list_of_scans = os.listdir(data_path)

    with open(csv_name, 'w', newline='') as csvfile:
        same = ['PatientID', 'Rows', 'Columns', 'BitsAllocated', 'BitsStored', 'RescaleIntercept', 'PixelSpacing']
        different = ['NumSlices', 'MinLocation', 'MaxLocation']
        writer = csv.DictWriter(csvfile, fieldnames=same+different)

        writer.writeheader()
        for scan in list_of_scans:#tqdm(list_of_scans):
            scan_path = os.path.join(data_path, scan)
            list_of_slices = os.listdir(scan_path)

            slice_path = os.path.join(scan_path, list_of_slices[0])

            ds = pydicom.read_file(slice_path) # you may have to use pydicom instead of dicom 

            row = {}
            for attr in same:
                row[attr] = ds.data_element(attr).value

            row['NumSlices'] = len(list_of_slices)

            key = []
            for slice in list_of_slices:
                slice_path = os.path.join(scan_path, slice)
                ds = pydicom.read_file(slice_path)
                try:
                    #key.append(ds.SliceLocation)
                    key.append(ds.ImagePositionPatient[2])
                except Exception as inst:
                    print(type(inst))    # the exception instance
                    print(inst.args)     # arguments stored in .args
                    print(inst)          # __str__ allows args to be printed directly,
                    print(ds)
                    print(ds.dir("position"))

            row['MinLocation'] = min(key)
            row['MaxLocation'] = max(key)

            ''' TODO
            depth = len(list_of_slices)
            order = np.argsort(key)
            assert(depth==len(order))
            '''

            writer.writerow(row)

write_stats('../data/stage2', 'stage2_stats.csv')

